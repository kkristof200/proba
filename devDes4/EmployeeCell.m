//
//  EmployeeCell.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 04. 24..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "EmployeeCell.h"

@implementation EmployeeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.profilePicture = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 64, 64)];
        [self customiseImageView:self.profilePicture];
        
        self.name = [[UILabel alloc]initWithFrame:CGRectMake(80, 8, self.bounds.size.width - 160, 28)];
        self.name.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
        [self customiseLabel:self.name];
        
        self.email = [[UILabel alloc]initWithFrame:CGRectMake(80, 44, self.bounds.size.width - 160, 28)];
        self.email.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f];
        [self customiseLabel:self.email];
        
        self.favorite = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 56, 16, 48, 48)];
        [self customiseImageView:self.favorite];
    }
    return self;
}

- (void)customiseImageView:(UIImageView *)image
{
    image.layer.masksToBounds = YES;
    image.layer.cornerRadius = image.frame.size.width / 2;
    image.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:image];
}

- (void)customiseLabel:(UILabel *)label
{
    label.textAlignment = NSTextAlignmentLeft;
    [self addSubview:label];
}

@end
