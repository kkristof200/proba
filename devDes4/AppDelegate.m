//
//  AppDelegate.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 06..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate()
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) UINavigationController *navDevelopers;
@property (strong, nonatomic) UINavigationController *navDesigners;
@property (strong, nonatomic) MainViewController *developers;
@property (strong, nonatomic) MainViewController *designers;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [UIWindow new];
    [self.window makeKeyAndVisible];
    self.window.frame = [[UIScreen mainScreen] bounds];
    
    self.tabBarController = [[UITabBarController alloc] init];
    [self.window setRootViewController:self.tabBarController];
    
    self.developers = [[MainViewController alloc]init];
    self.developers.title = @"Developers";
    self.designers = [[MainViewController alloc]init];
    self.designers.title = @"Designers";
    
    self.navDevelopers = [[UINavigationController alloc] initWithRootViewController:self.developers];
    self.navDesigners = [[UINavigationController alloc] initWithRootViewController:self.designers];
    
    NSArray* controllers = [NSArray arrayWithObjects:self.navDevelopers, self.navDesigners, nil];
    self.tabBarController.viewControllers = controllers;
    
    return YES;
}

@end
