//
//  DataLayer.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 26..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "DataLayer.h"
#import "Developer.h"
#import "Designer.h"

@implementation DataLayer

+ (DataLayer*) sharedInstance
{
    static DataLayer *myInstance = nil;
    if (myInstance == nil)
    {
        myInstance = [[self alloc] init];
    }
    return myInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.developers = [[NSMutableArray alloc] init];
        self.designers = [[NSMutableArray alloc] init];
        
        Developer *developer1 = [[Developer alloc] init];
        developer1.name = @"Quentin Tarantino";
        developer1.email = @"barnes@noble7.com";
        developer1.profilePictureURL = @"https://pbs.twimg.com/profile_images/1178926649/QuentinTarantino.jpeg";
        developer1.platformEnum = android;
        [self.developers addObject:developer1];
        
        Developer *developer2 = [[Developer alloc] init];
        developer2.name = @"Oliver Heldens";
        developer2.email = @"Oli@Held.com";
        developer2.profilePictureURL = @"https://pbs.twimg.com/profile_images/463991983879385088/enPw0yL0_400x400.jpeg";
        developer2.platformEnum = apple;
        [self.developers addObject:developer2];
        
        Designer *designer1 = [[Designer alloc] init];
        designer1.name = @"Leonardo DiCaprio";
        designer1.email = @"Leo@TheDeparted.com";
        designer1.profilePictureURL = @"http://www.hollywoodreporter.com/sites/default/files/imagecache/675x380/2014/11/leonardo_dicaprio.jpg";
        designer1.colorEnum = windows;
        [self.designers addObject:designer1];
    }
    return self;
}

@end
