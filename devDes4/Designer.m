//
//  Designer.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 19..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "Designer.h"

@implementation Designer

- (UIImage *)colorPicture
{
    NSString * colorImageName;
    
    switch (self.colorEnum)
    {
        case color1:
            colorImageName = @"color1";
            break;
        case color2:
            colorImageName = @"color2";
            break;
        case color3:
            colorImageName = @"color3";
            break;
        case color4:
            colorImageName = @"color4";
            break;
        case color5:
            colorImageName = @"color5";
            break;
            
        default:
            break;
    }
    
    return [UIImage imageNamed:colorImageName];
}

@end
