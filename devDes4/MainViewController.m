//
//  MainViewController.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 08..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "MainViewController.h"
#import "ProfileViewController.h"
#import "Developer.h"
#import "Designer.h"
#import "EmployeeCell.h"
#import "DataLayer.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) AddEditDesignersAndDevelopers *addVC;
@property (strong, nonatomic) ProfileViewController *profileVC;

@property UITableView *tableView;
@property UIBarButtonItem *addBarButton;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] initWithFrame: self.view.bounds style:UITableViewStylePlain];
    self.tableView.rowHeight = 80;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView ];
    
    self.addBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(pushAddViewController:)];
    
    self.navigationItem.leftBarButtonItem = self.addBarButton;
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTableView:)
                                                 name:@"reloadTableView"
                                               object:nil];
}

-(IBAction)pushAddViewController:(id)sender
{
    self.addVC = [[AddEditDesignersAndDevelopers alloc]init];
    self.addVC.title = @"Add";
    [self.navigationController pushViewController:self.addVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.tabBarController.selectedIndex == 0){
        return [DataLayer sharedInstance].developers.count;
    }
    return [DataLayer sharedInstance].designers.count;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"\n\nLoaded\n");
    static NSString *cellIdentifier = @"cellID";
    
    EmployeeCell *cell = (EmployeeCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[EmployeeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Employee *employee;
    if (self.tabBarController.selectedIndex == 0)
    {
        Developer *developer = [[DataLayer sharedInstance].developers objectAtIndex:indexPath.row];
        employee = developer;
        
        cell.favorite.image = developer.platformPicture;
    }
    else
    {
        Designer *designer = [[DataLayer sharedInstance].designers objectAtIndex:indexPath.row];
        employee = designer;
        
        cell.favorite.image = designer.colorPicture;
    }
    
    if(employee.profilePicture!=nil) cell.profilePicture.image = employee.profilePicture;
    else
    {
        NSLog(@"\n\nDownloaded Image %ld\n",(long)indexPath.row);
        [self saveImageReference:employee forcell:cell];
    }
    
    cell.name.text = employee.name;
    cell.email.text = employee.email;
    
        return cell;
}

- (void)saveImageReference:(Employee *)employee forcell:(EmployeeCell *)cell
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {

        employee.profilePicture = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:employee.profilePictureURL]]];
        
        
        dispatch_sync(dispatch_get_main_queue(), ^(void) {
            [self postNotificationToReloadProfilePicture];
            cell.profilePicture.image = employee.profilePicture;
        });
    });
}

- (void) postNotificationToReloadProfilePicture
{
    
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"reloadProfilePicture"
     object:self];
    
}

/*
- (void)saveImageForEmployee:(Employee *)employee atIndex:(NSIndexPath *)indexPath forCell:(EmployeeCell *)cell
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
        UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:employee.profilePicture]]];
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath;
        if (self.tabBarController.selectedIndex == 0)pngFilePath = [NSString stringWithFormat:@"%@/developer%ld.png",docDir, (long)indexPath.row];
        else pngFilePath = [NSString stringWithFormat:@"%@/designer%ld.png",docDir, (long)indexPath.row];
        NSData *data = [NSData dataWithData:UIImagePNGRepresentation(image)];
        [data writeToFile:pngFilePath atomically:YES];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"TestNotification" object:self];
        
        dispatch_sync(dispatch_get_main_queue(), ^(void) {
            cell.profilePicture.image = image;
        });
    });
 }

- (void)saveImageToUserDefaults:(Employee *)employee forCell:(EmployeeCell *)cell
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
    NSData * data;
    UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:employee.profilePicture]]];
    
    data = UIImageJPEGRepresentation(image, 1.0);
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:employee.profilePicture];
    [userDefaults synchronize];
        
        dispatch_sync(dispatch_get_main_queue(), ^(void) {
            cell.profilePicture.image = image;
        });
    });
}
*/

-(UIImage *)loadImageFromUserDefaultsForKey:(NSString *)key
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    return [UIImage imageWithData:[userDefaults objectForKey:key]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    long index = indexPath.row;
    if (self.tabBarController.selectedIndex == 0){
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            [[DataLayer sharedInstance].developers removeObjectAtIndex:index];
            
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        }
    }
    else{
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            // Delete the row from the data source
            [[DataLayer sharedInstance].designers removeObjectAtIndex:index];
            
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }
        else if (editingStyle == UITableViewCellEditingStyleInsert) {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
    if (editing)
    {
        // you might disable other widgets here... (optional)
    } else {
        // re-enable disabled widgets (optional)
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long selectedRow = indexPath.row;
    NSLog(@"indexPath.row = %ld",selectedRow);
    
    Employee *employee;
    if(self.tabBarController.selectedIndex==0) employee = [[DataLayer sharedInstance].developers objectAtIndex:indexPath.row];
    else employee = [[DataLayer sharedInstance].designers objectAtIndex:indexPath.row];
    
    ProfileViewController *profileVC = [[ProfileViewController alloc] init];
    [profileVC setEmployee:employee];
    
    NSLog(@"indPath1.row = %ld",profileVC.indPath);
    profileVC.title = @"Profile";
    [self.navigationController pushViewController:profileVC animated:YES];
    NSLog(@"Is Loaded");
}

- (void) reloadTableView:(NSNotification *) notification
{
    [self.tableView reloadData];
}

@end

