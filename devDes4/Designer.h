//
//  Designer.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 19..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"

typedef NS_ENUM(NSInteger, colorEnum)
{
    color1,
    color2,
    color3,
    color4,
    color5
};

@interface Designer : Employee

@property (readonly) UIImage *colorPicture;


@property (nonatomic, assign) colorEnum colorEnum;


@end
