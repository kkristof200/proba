//
//  AddEditBoth.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 12..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "AddEditDesignersAndDevelopers.h"
#import "Developer.h"
#import "Designer.h"
#import "DataLayer.h"

@interface AddEditDesignersAndDevelopers () <UITextFieldDelegate>

@property platformEnum selectedPlatformEnum;
@property colorEnum selectedColorEnum;

@property UITextField *firstNameTextField;
@property UITextField *lastNameTextField;
@property UITextField *emailTextField;
@property UITextField *photoTextField;

@property UIButton *prepareToSelectFavorite;

@property UIImageView *selectedPlatformOrColor;

@property UIVisualEffectView *blur;

@property UIBarButtonItem *save;

@end

@implementation AddEditDesignersAndDevelopers

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    const CGFloat spaceBetweenTextFields = 5;
    CGRect frame = CGRectMake(5, 70, self.view.frame.size.width - 10, 50);
    
    
    self.firstNameTextField = [[UITextField alloc] initWithFrame:frame];
    [self customizeTextfield:self.firstNameTextField];
    self.firstNameTextField.placeholder = @"Enter First Name";
    
    frame.origin.y += frame.size.height + spaceBetweenTextFields;
    self.lastNameTextField = [[UITextField alloc] initWithFrame:frame];
    [self customizeTextfield:self.lastNameTextField];
    self.lastNameTextField.placeholder = @"Enter Last Name";
    
    frame.origin.y += frame.size.height + spaceBetweenTextFields;
    self.emailTextField = [[UITextField alloc] initWithFrame:frame];
    [self customizeTextfield:self.emailTextField];
    self.emailTextField.placeholder = @"Enter Email";
    
    frame.origin.y += frame.size.height + spaceBetweenTextFields;
    self.photoTextField = [[UITextField alloc] initWithFrame:frame];
    [self customizeTextfield:self.photoTextField];
    self.photoTextField.placeholder = @"Enter Photo URL";
    
    
    frame.origin.y += frame.size.height + spaceBetweenTextFields;
    self.prepareToSelectFavorite = [[UIButton alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 120) / 2, frame.origin.y, 120, 30)];
    [self.prepareToSelectFavorite addTarget:self action:@selector(selectFavorite:) forControlEvents:UIControlEventTouchUpInside];
    [self.prepareToSelectFavorite setTitle:@"Select Favorite" forState:UIControlStateNormal];
    self.prepareToSelectFavorite.backgroundColor = [UIColor colorWithRed:(240/255.0) green:(94/255.0) blue:(85/255.0) alpha:1];
    [self.view addSubview:self.prepareToSelectFavorite];
    
    
    frame.origin.y += frame.size.height + spaceBetweenTextFields;
    self.selectedPlatformOrColor = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 50) / 2, frame.origin.y, 50, 50)];
    self.selectedPlatformOrColor.layer.masksToBounds = YES;
    self.selectedPlatformOrColor.layer.cornerRadius = self.selectedPlatformOrColor.bounds.size.width / 2;
    [self.view addSubview:self.selectedPlatformOrColor];
    
    
    self.blur = [[UIVisualEffectView alloc]initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    self.blur.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.blur];
    
    
    self.save = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save:)];
    self.navigationItem.rightBarButtonItem = self.save;
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.tabBarController.tabBar.hidden = YES;
    self.tabBarController.tabBar.opaque = YES;
}

- (void)customizeTextfield:(UITextField *)textField
{
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.backgroundColor = [UIColor colorWithRed:(247/255.0) green:(246/255.0) blue:(243/255.0) alpha:1];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:textField];
}

- (IBAction)selectFavorite:(id)sender
{
    [self hideKeyboardBecauseOfSelect];
    const CGFloat buttonWidthHeight = 50;
    CGFloat spaceBetweenButtons = 10;
    const CGFloat spaceToEdge = 10;
    
    int totalSpaceOfButtonsWithDistance = (self.view.frame.size.width - spaceToEdge - (spaceToEdge - spaceBetweenButtons));
    int buttonLengthWithDistance = buttonWidthHeight + spaceBetweenButtons;
    
    int columns = totalSpaceOfButtonsWithDistance / buttonLengthWithDistance;
    float rest = totalSpaceOfButtonsWithDistance % buttonLengthWithDistance;
    
    CGRect favoriteFrame = CGRectMake((spaceToEdge), (65 + spaceToEdge), buttonWidthHeight, buttonWidthHeight);
    
    [UIView animateWithDuration:0.35
                          delay:0.0
                        options: UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [self.blur setFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                     }];
    
    int numberOfButtons;
    NSString *nameOfTheButtonImage;
    
    if(self.tabBarController.selectedIndex==0)
    {
        numberOfButtons = 3;
        nameOfTheButtonImage = @"platform";
    }
    else
    {
        numberOfButtons = 5;
        nameOfTheButtonImage = @"color";
    }
    
    
    if (numberOfButtons<columns)
    {
        rest = self.view.frame.size.width - (2*spaceToEdge) - (numberOfButtons * buttonWidthHeight);
        columns = numberOfButtons;
        spaceBetweenButtons = rest / (columns - 1);
    }
    else if(rest > 0) spaceBetweenButtons += rest / (columns - 1);
    
    
    for (int i = 1 ; i <= numberOfButtons ; i++)
    {
        UIImageView *bgImage = [[UIImageView alloc]init];
        [self setImage:bgImage withName:nameOfTheButtonImage withNumber:(long)i];
        NSLog(@"%@",bgImage.image.description);
        UIButton *newButton = [[UIButton alloc] initWithFrame:favoriteFrame];
        [newButton setBackgroundImage:bgImage.image forState:UIControlStateNormal];
        newButton.layer.cornerRadius = buttonWidthHeight / 2;
        newButton.tag = i;
        [self customizeButton:newButton];
        favoriteFrame.origin.x += buttonWidthHeight + spaceBetweenButtons;
        if(i % columns == 0){
            favoriteFrame.origin.y += buttonWidthHeight + spaceBetweenButtons;
            favoriteFrame.origin.x = spaceToEdge;
        }
    }
}

- (void)customizeButton:(UIButton *)button
{
    button.layer.masksToBounds = YES;
    [button addTarget:self action:@selector(favoriteWasSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.blur addSubview:button];
}

- (IBAction)favoriteWasSelected:(id)sender
{
    if(self.tabBarController.selectedIndex==0){
        [self setImage:self.selectedPlatformOrColor withName:@"platform" withNumber:(long)[sender tag]];
        self.selectedPlatformEnum = [sender tag];
        NSLog(@"selectedPlatformEnum = %ld", (long)[sender tag]);
    }
    else{
        [self setImage:self.selectedPlatformOrColor withName:@"color" withNumber:(long)[sender tag]];
        self.selectedColorEnum = [sender tag];
        NSLog(@"selectedColorEnum = %ld", (long)[sender tag]);
    }
    
    [UIView animateWithDuration:0.35
                          delay:0.0
                        options: UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         [self.blur setFrame: CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void)setImage:(UIImageView *)imageView withName:(NSString *)name withNumber:(long)number
{
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%ld",name,number]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
    self.tabBarController.tabBar.opaque = NO;
}

- (IBAction)save:(id)sender
{
    if(self.firstNameTextField.text.length>0 && self.lastNameTextField.text.length>0 && self.emailTextField.text.length>0 && self.photoTextField.text.length>0 && (self.selectedPlatformOrColor.image != nil))
    {
        NSString *fullName = [[self.firstNameTextField.text stringByAppendingString:@" "] stringByAppendingString:
                              self.lastNameTextField.text];
        
        if(self.tabBarController.selectedIndex==0){
            Developer *newDeveloper = [[Developer alloc] init];
        
            newDeveloper.platformEnum = self.selectedPlatformEnum-1;
            newDeveloper.name = fullName;
            newDeveloper.email = self.emailTextField.text;
            newDeveloper.profilePictureURL = self.photoTextField.text;
            [[DataLayer sharedInstance].developers addObject:newDeveloper];
        }
        else{
            Designer *newDesigner = [[Designer alloc] init];
            
            newDesigner.colorEnum = self.selectedColorEnum-1;
            newDesigner.name = fullName;
            newDesigner.email = self.emailTextField.text;
            newDesigner.profilePictureURL = self.photoTextField.text;
            [[DataLayer sharedInstance].designers addObject:newDesigner];
        }
        [self postNotificationToReloadTableView];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self userDidntFillAllTheBlanks];
    }
}

- (void) postNotificationToReloadTableView
{
    
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"reloadTableView"
     object:self];
    
}

- (void)userDidntFillAllTheBlanks
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Warning!"
                                                   message: @"Fill all the blanks, and choose favorite\n color or platform!"
                                                  delegate: self
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    [alert show];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [(UITextField*)textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.firstNameTextField isFirstResponder] || [self.lastNameTextField isFirstResponder] || [self.emailTextField isFirstResponder] || [self.photoTextField isFirstResponder])
    {
        [self hideKeyboardBecauseOfSelect];
    }
}

-(void)hideKeyboardBecauseOfSelect
{
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.photoTextField resignFirstResponder];
}

@end