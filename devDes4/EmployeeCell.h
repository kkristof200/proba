//
//  EmployeeCell.j
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 04. 24..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeCell : UITableViewCell

@property (strong, nonatomic)UILabel *name;
@property (strong, nonatomic)UILabel *email;
@property (strong, nonatomic)UIImageView *profilePicture;
@property (strong, nonatomic)UIImageView *favorite;

@end
