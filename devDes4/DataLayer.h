//
//  DataLayer.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 26..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataLayer : NSObject

@property NSMutableArray *developers;
@property NSMutableArray *designers;
@property long tableViewTag;

+ (DataLayer*) sharedInstance;

@end
