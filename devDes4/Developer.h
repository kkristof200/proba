//
//  Developer.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 19..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"

typedef NS_ENUM(NSInteger, platformEnum)
{
    android,
    apple,
    windows
};

@interface Developer : Employee

@property (readonly) UIImage *platformPicture;


@property (nonatomic, assign) platformEnum platformEnum;

@end
