//
//  AppDelegate.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 06..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "AddEditDesignersAndDevelopers.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

