//
//  main.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 06..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
