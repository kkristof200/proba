//
//  ProfileViewController.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 13..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Employee.h"
#import "Designer.h"
#import "Developer.h"

@interface ProfileViewController : UIViewController

@property (nonatomic) long indPath;
@property Employee *employee;
@property Developer *developer;
@property Designer *designer;

@end
