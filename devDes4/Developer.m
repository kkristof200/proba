//
//  Developer.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 19..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "Developer.h"

@implementation Developer

- (UIImage *)platformPicture
{
    NSString * platformImageName;
    
    switch (self.platformEnum)
    {
        case android:
            platformImageName = @"platform1";
            break;
        case apple:
            platformImageName = @"platform2";
            break;
        case windows:
            platformImageName = @"platform3";
            break;
            
        default:
            break;
    }
    
    return [UIImage imageNamed:platformImageName];
}

@end
