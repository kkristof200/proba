//
//  Employee.h
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 25..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Employee : NSObject

@property NSString *name;
@property NSString *email;
@property NSString *profilePictureURL;

@property UIImage *profilePicture;

@end
