//
//  ProfileViewController.m
//  devDes4
//
//  Created by Kovács Kristóf on 2015. 03. 13..
//  Copyright (c) 2015. Kovács Kristóf. All rights reserved.
//

#import "ProfileViewController.h"
#import "MainViewController.h"
#import "Developer.h"
#import "Designer.h"
#import "DataLayer.h"

@interface ProfileViewController()

@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UIImageView *favoriteColorOrPlatform;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *developerOrDesigner;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@end

@implementation ProfileViewController
@synthesize indPath;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat spaceBetweenObjects = 8;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 165)];
    [self.view addSubview:self.bgImage];
    
    self.profilePicture = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 65, 100, 130, 130)];
    self.profilePicture.layer.masksToBounds = YES;
    self.profilePicture.layer.cornerRadius = 65;
    [self.profilePicture.layer setBorderWidth:2];
    [self.profilePicture.layer setBorderColor:[[UIColor whiteColor] CGColor ]];
    [self customizeImage:self.profilePicture];
    
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.profilePicture.frame.origin.y + self.profilePicture.frame.size.height + spaceBetweenObjects, self.view.bounds.size.width, 30)];
    self.nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f];
    [self customizeLabel:self.nameLabel];
    
    self.emailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height + spaceBetweenObjects, self.view.bounds.size.width, 30)];
    self.emailLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    [self customizeLabel:self.emailLabel];
    
    self.favoriteColorOrPlatform = [[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2) - 50,  self.emailLabel.frame.origin.y + self.emailLabel.frame.size.height + spaceBetweenObjects, 30, 30)];
    [self customizeImage:self.favoriteColorOrPlatform];
    
    self.developerOrDesigner = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2) - 12, self.favoriteColorOrPlatform.frame.origin.y, 62, 30)];
    self.developerOrDesigner.text = [NSString stringWithFormat:@"%ld",self.indPath];
    self.developerOrDesigner.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
    [self customizeLabel:self.developerOrDesigner];
    
    if ([self.employee isKindOfClass:[Developer class]])
    {
        Developer *developer = (Developer *)self.employee;
        self.developerOrDesigner.text = @"Developer";
        self.favoriteColorOrPlatform.image = developer.platformPicture;
        int rand = arc4random()%5+1;
        self.bgImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"color%i",rand]];
    }
    else
    {
        Designer *designer = (Designer *)self.employee;;
        self.developerOrDesigner.text = @"Designer";
        self.favoriteColorOrPlatform.layer.masksToBounds = YES;
        self.favoriteColorOrPlatform.layer.cornerRadius = 15;
        self.favoriteColorOrPlatform.image = designer.colorPicture;
        self.bgImage.image = designer.colorPicture;
    }
    
    if(self.employee.profilePicture!=nil) self.profilePicture.image = self.employee.profilePicture;
    
    self.nameLabel.text = self.employee.name;
    self.emailLabel.text = self.employee.email;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadProfilePicture:)
                                                 name:@"reloadProfilePicture"
                                               object:nil];
}

- (void)customizeLabel:(UILabel *)label
{
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
}

- (void)customizeImage:(UIImageView *)imageView
{
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:imageView];
}

- (void) reloadProfilePicture:(NSNotification *) notification
{
    self.profilePicture.image = self.employee.profilePicture;
}

@end